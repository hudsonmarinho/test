import _ from 'lodash'

const state = {
  isLoading: true,
  orderBy: 'score',
  productList: require('../../../data/products.json')
}

const mutations = {
  'UPDATE_PRODUCT_LIST' (state, productList) {
    state.productList = productList;
    state.isLoading = false;
  }
}

const actions = {
}

const getters = {
  products: (state) => {
    return _.orderBy(state.productList, state.orderBy, 'asc');
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
