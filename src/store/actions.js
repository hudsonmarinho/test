export const updateCart = ({ commit }, { item, quantity, isAdd }) => {
  commit('UPDATE_CART', { item, quantity, isAdd })

  if(isAdd){
    let message_obj = {
      message: `<strong>${item.name}</strong> adicionado ao carrinho com sucesso!`,
      messageClass: "success",
      autoClose: true
    }

    commit('ADD_MESSAGE', message_obj)
  }
}

export const removeItemInCart = ({ commit }, { item }) => {
  let message_obj = {
    message: `<strong>${item.name}</strong> removido do carrinho!`,
    messageClass: "warning",
    autoClose: true
  }

  commit('REMOVE_CART_ITEM', { item });
  commit('ADD_MESSAGE', message_obj)
}
