import Firebase from 'firebase'
import 'firebase/firestore'

const config = {
  apiKey: "AIzaSyCI_5EhVmfr5lVNYenX_IjTQLsZfHXRhAw",
  authDomain: "cr-test-83241.firebaseapp.com",
  databaseURL: "https://cr-test-83241.firebaseio.com",
  projectId: "cr-test-83241",
  storageBucket: "cr-test-83241.appspot.com",
  messagingSenderId: "807794930973"
}

const firebaseApp = Firebase.initializeApp(config)
const api = firebaseApp.firestore()

api.settings({
  timestampsInSnapshots: true
})

export default api
